package ag.pst.test.stream.sort;

import java.util.function.Function;
import java.util.stream.Stream;

public class SortStream<T> {
	public Stream<T> sortWithoutStoringInMemory(Stream<T> unsortedStream, Function<T, String> uniqueKeyGetter) throws Exception {
		// implement your logic here
		return unsortedStream;
	}

	public Stream<T> sortByStoringInMemory(Stream<T> unsortedStream) {
		return unsortedStream.sorted();
	}
}