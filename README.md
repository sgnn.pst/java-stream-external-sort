Stream sorting programming task
- Implement method sortWithoutStoringInMemory in SortStream class so that:
1. It returns a sorted Stream based on the input Stream and Function.
2. Memory consumption when running tests should not exceed 1 GB (You can re-enable testSortByStoringInMemory method and check memory consumption for reference).
3. Speed of execution is not important for this task.
- This method receives a Stream of Generic, a Generic Function for retrieving a unique key from a given object in the stream (should be used for sorting logic).
- Unit tests are already included. Simply run "mvn test" to test your implementation.
- You are free to create additional classes and/or use external dependencies.
- You are free to modify the class SortStream, except for the Generic output of the method sortWithoutStoringInMemory
- You can make changes to the unit tests where necessary.
- You can make assumptions about the input data where necessary.

Please send your implementation via email.
